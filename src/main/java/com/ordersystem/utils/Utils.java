/**
 * @author Nil Panchal
 * Mex IT
 * Jun 26, 2022
 * 3:51:16 PM
 * Utils.java
 * com.ordersystem.utils
 * OrderSystem
 */
package com.ordersystem.utils;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * @author Nil Panchal
 * Mex IT
 * Jun 26, 2022
 * 3:51:16 PM
 */
public class Utils {
  public static String getShortId(String initials) {
    return initials + RandomStringUtils.randomAlphanumeric(5);
  }
  
  public static String convertDateTimeIntoString(Instant dateTime) {
    return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
        .withZone(ZoneId.of("Europe/Berlin")).format(dateTime);
  }
}
