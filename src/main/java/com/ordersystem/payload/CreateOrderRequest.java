/**
 * @author Nil Panchal Mex IT Jun 26, 2022 12:20:19 PM CreateOrderRequest.java
 *         com.ordersystem.payload OrderSystem
 */
package com.ordersystem.payload;

import static com.ordersystem.constant.Messages.RANGE_QUANTITY_ERROR_MESSAGE;
import static com.ordersystem.constant.Messages.RANGE_CLIENT_ID_ERROR_MESSAGE;
import org.hibernate.validator.constraints.Range;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Nil Panchal Mex IT Jun 26, 2022 12:20:19 PM
 */
@Getter
@Setter
public class CreateOrderRequest {

  @Range(min = 1, max = 20000, message = RANGE_CLIENT_ID_ERROR_MESSAGE)
  private int customerId;

  @Range(min = 1, max = 50, message = RANGE_QUANTITY_ERROR_MESSAGE)
  private int quantity;
}
