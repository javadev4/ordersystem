/**
 * @author Nil Panchal Mex IT Jun 26, 2022 6:26:43 PM OrderResponse.java
 *         com.ordersystem.payload.response OrderSystem
 */
package com.ordersystem.payload.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Nil Panchal Mex IT Jun 26, 2022 6:26:43 PM
 */
@Getter
@Setter
@Builder
public class OrderResponse {
  private String orderId;
  private long customerId;
  private long quantity;
  private String createdAt;
  private long waitingTime;
}
