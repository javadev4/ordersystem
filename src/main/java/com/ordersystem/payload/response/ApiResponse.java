/**
 * @author Nil Panchal Mex IT Jun 26, 2022 4:21:54 PM ApiResponse.java
 *         com.ordersystem.payload.response OrderSystem
 */
package com.ordersystem.payload.response;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Nil Panchal Mex IT Jun 26, 2022 4:21:54 PM
 */
@Getter
@Setter
@Builder
public class ApiResponse {
  private int code;
  private String status;
  private String message;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private OrderResponse order;
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private List<OrderResponse> orders;
}
