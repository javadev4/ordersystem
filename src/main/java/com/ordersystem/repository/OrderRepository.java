/**
 * @author Nil Panchal Mex IT Jun 26, 2022 4:02:41 PM OrderRepository.java
 *         com.ordersystem.repository OrderSystem
 */
package com.ordersystem.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Repository;
import com.ordersystem.model.Order;

/**
 * @author Nil Panchal Mex IT Jun 26, 2022 4:02:41 PM
 */
@Repository
public interface OrderRepository extends BaseRepository<Order> {
  Optional<Order> findByCustomerId(int customerId);

  List<Order> findByCompleted(boolean completed);

  Optional<Order> findByCustomerIdAndCompleted(int customerId, boolean completed);
}
