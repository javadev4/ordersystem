/**
 * @author Nil Panchal Mex IT Jun 26, 2022 4:03:10 PM BaseRepository.java com.ordersystem.repository
 *         OrderSystem
 */
package com.ordersystem.repository;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import com.ordersystem.model.BaseEntity;

/**
 * @author Nil Panchal Mex IT Jun 26, 2022 4:03:10 PM
 */
@NoRepositoryBean
public interface BaseRepository<T extends BaseEntity> extends JpaRepository<T, UUID> {

  Optional<T> findById(UUID id);
}
