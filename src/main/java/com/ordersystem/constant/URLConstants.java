/**
 * @author Nil Panchal Mex IT Jun 26, 2022 12:22:57 PM URLConstants.java com.ordersystem.constant
 *         OrderSystem
 */
package com.ordersystem.constant;

/**
 * @author Nil Panchal Mex IT Jun 26, 2022 12:22:57 PM
 */
public class URLConstants {

  public static final String ORDERS = "/orders";
  public static final String SINGLE_ORDER = ORDERS + "/{" + PathVariableConstants.CLIENT_ID + "}";
  public static final String SINGLE_ORDER_STATUS = SINGLE_ORDER + "/status";
  public static final String ORDERS_DELIVERY = ORDERS + "/delivery";
}
