/**
 * @author Nil Panchal
 * Mex IT
 * Jun 26, 2022
 * 12:24:18 PM
 * PathVariableConstants.java
 * com.ordersystem.constant
 * OrderSystem
 */
package com.ordersystem.constant;

/**
 * @author Nil Panchal
 * Mex IT
 * Jun 26, 2022
 * 12:24:18 PM
 */
public class PathVariableConstants {
  public static final String CLIENT_ID = "clientId";
}
