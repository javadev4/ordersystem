/**
 * @author Nil Panchal Mex IT Jun 26, 2022 1:10:13 PM Messages.java com.ordersystem.constant
 *         OrderSystem
 */
package com.ordersystem.constant;

/**
 * @author Nil Panchal Mex IT Jun 26, 2022 1:10:13 PM
 */
public class Messages {
  public static final String RANGE_CLIENT_ID_ERROR_MESSAGE =
      "Client id should be between 0 to 20000";
  public static final String RANGE_QUANTITY_ERROR_MESSAGE = "Quantity should be between 1 to 50";

  public static final String ORDER_CREATED_SUCCESSFULLY = "Order created successfully";

  public static final String ORDER_RETRIEVED_SUCCESSFULLY = "All orders retrived successfully";

  public static final String NEXT_ORDER_RETRIEVED_SUCCESSFULLY =
      "Next delivery orders retrived successfully";
  public static final String ORDER_CANCELLED_SUCCESSFULLY = "Order cancelled successfully";

  public static final String SUCCESS = "Success";
  public static final String FAIL = "Fail";
}
