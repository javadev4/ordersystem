/**
 * @author Nil Panchal Mex IT Jun 26, 2022 12:43:04 PM OrderService.java com.ordersystem.service
 *         OrderSystem
 */
package com.ordersystem.service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.ordersystem.comparator.OrderComparator;
import com.ordersystem.constant.Messages;
import com.ordersystem.enums.Priority;
import com.ordersystem.exception.AppException;
import com.ordersystem.model.Order;
import com.ordersystem.payload.CreateOrderRequest;
import com.ordersystem.payload.response.ApiResponse;
import com.ordersystem.payload.response.OrderResponse;
import com.ordersystem.repository.OrderRepository;
import com.ordersystem.utils.Utils;

/**
 * @author Nil Panchal Mex IT Jun 26, 2022 12:43:04 PM
 */
@Service
public class OrderService {

  private static final Logger log = LoggerFactory.getLogger(OrderService.class);
  private OrderRepository orderRepository;

  private final int approxWaitTimePerDoner = 1;

  @Autowired
  public OrderService(OrderRepository orderRepository) {
    this.orderRepository = orderRepository;
  }

  public ApiResponse createOrder(CreateOrderRequest request) {
    log.info("createOrder ->");
    validateRequest(request);
    Order order = orderRepository
        .save(Order.builder().shortId(Utils.getShortId("odr")).customerId(request.getCustomerId())
            .quantity(request.getQuantity()).priority(getPriority(request)).build());
    List<OrderResponse> orders = getOrders();
    OrderResponse orderResponse = orders.stream().reduce((first, second) -> second).orElse(null);
    return ApiResponse.builder().code(HttpStatus.CREATED.value()).status(Messages.SUCCESS)
        .message(Messages.ORDER_CREATED_SUCCESSFULLY)
        .order(
            OrderResponse.builder().customerId(order.getCustomerId()).quantity(order.getQuantity())
                .createdAt(Utils.convertDateTimeIntoString(order.getCreatedAt()))
                .waitingTime(orderResponse.getWaitingTime()).orderId(order.getShortId()).build())
        .build();
  }

  /**
   * To validate create order request
   */
  private void validateRequest(CreateOrderRequest request) {
    log.info("validateRequest ->");
    Optional<Order> order =
        orderRepository.findByCustomerIdAndCompleted(request.getCustomerId(), false);
    if (order.isPresent()) {
      throw AppException.customerOrderExistsException();
    }
  }

  /**
   * To read order queue
   */
  public ApiResponse getOrdersQueue() {
    log.info("getOrdersQueue ->");
    return ApiResponse.builder().code(HttpStatus.OK.value()).status(Messages.SUCCESS)
        .message(Messages.ORDER_RETRIEVED_SUCCESSFULLY).orders(getOrders()).build();
  }

  /**
   * To get all orders
   */
  private List<OrderResponse> getOrders() {
    log.info("getOrders ->");
    List<Order> allOrders = orderRepository.findByCompleted(false);
    PriorityQueue<Order> queue = new PriorityQueue<>(new OrderComparator().reversed());
    queue.addAll(allOrders);

    log.info("Elements in queue: " + queue);
    AtomicInteger totalWaitingTime = new AtomicInteger(1);
    return IntStream.iterate(0, i -> i + 1).limit(queue.size()).mapToObj(i -> {
      Order order = queue.poll();
      log.info(order.toString());
      totalWaitingTime.getAndAdd(order.getQuantity() * approxWaitTimePerDoner);
      return OrderResponse.builder().orderId(order.getShortId()).customerId(order.getCustomerId())
          .quantity(order.getQuantity())
          .createdAt(Utils.convertDateTimeIntoString(order.getCreatedAt()))
          .waitingTime(totalWaitingTime.get()).build();
    }).collect(Collectors.toList());
  }


  /**
   * To get customer order status
   */
  public ApiResponse getCustomerOrderStatus(int customerId) {
    log.info("getCustomerOrderStatus ->");
    OrderResponse orderResponse =
        getOrders().stream().filter(data -> data.getCustomerId() == customerId).findFirst().get();
    return ApiResponse.builder().code(HttpStatus.CREATED.value()).status(Messages.SUCCESS)
        .message(Messages.ORDER_RETRIEVED_SUCCESSFULLY)
        .order(OrderResponse.builder().customerId(orderResponse.getCustomerId())
            .quantity(orderResponse.getQuantity()).createdAt(orderResponse.getCreatedAt())
            .waitingTime(orderResponse.getWaitingTime()).orderId(orderResponse.getOrderId())
            .build())
        .build();
  }

  /**
   * To get next delivery orders
   */
  public ApiResponse getNextDeliveryOrders() {
    log.info("getNextDeliveryOrders ->");
    List<Order> allOrders = orderRepository.findByCompleted(false);
    PriorityQueue<Order> queue = new PriorityQueue<>(new OrderComparator().reversed());
    queue.addAll(allOrders);
    AtomicInteger cartSize = new AtomicInteger(50);
    boolean isCartFull = false;
    List<Order> updateOrders = new ArrayList<>();

    while (!isCartFull && !queue.isEmpty()) {
      Order order = queue.poll();
      log.info("Cart : " + cartSize.get());
      if (cartSize.get() >= order.getQuantity()) {
        updateOrders.add(order.toBuilder().completed(true).completedAt(Instant.now()).build());
        cartSize.set(cartSize.get() - order.getQuantity());
      } else if (cartSize.get() < order.getQuantity()) {
        isCartFull = true;
      }
    }

    List<OrderResponse> orders = updateOrders.stream().map(order -> {
      return OrderResponse.builder().orderId(order.getShortId()).customerId(order.getCustomerId())
          .quantity(order.getQuantity())
          .createdAt(Utils.convertDateTimeIntoString(order.getCreatedAt()))
          .waitingTime(ChronoUnit.MINUTES.between(order.getCreatedAt(), Instant.now())).build();
    }).collect(Collectors.toList());

    orderRepository.saveAll(updateOrders);
    return ApiResponse.builder().code(HttpStatus.OK.value()).status(Messages.SUCCESS).orders(orders)
        .message(Messages.NEXT_ORDER_RETRIEVED_SUCCESSFULLY).build();
  }

  /**
   * To cancel order
   */
  public ApiResponse cancelCustomerOrder(int clientId) {
    log.info("cancelCustomerOrder ->");
    Order order = orderRepository.findByCustomerId(clientId).get();
    orderRepository.save(order.toBuilder().completedAt(Instant.now()).completed(true).build());
    return ApiResponse.builder().code(HttpStatus.OK.value()).status(Messages.SUCCESS)
        .message(Messages.ORDER_CANCELLED_SUCCESSFULLY).build();
  }


  /**
   * Function to determine priority
   */
  private Priority getPriority(CreateOrderRequest request) {
    log.info("getPriority ->");
    if (request.getCustomerId() < 1000) {
      return Priority.HIGH;
    }
    return Priority.LOW;
  }
}
