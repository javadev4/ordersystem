/**
 * @author Nil Panchal Mex IT Jun 26, 2022 3:44:33 PM Order.java com.ordersystem.model OrderSystem
 */
package com.ordersystem.model;

import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import com.ordersystem.enums.Priority;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

/**
 * @author Nil Panchal Mex IT Jun 26, 2022 3:44:33 PM
 */

@Getter
@Setter
@Entity(name = "oder")
@Table(name = "oder")
@ToString
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
public class Order extends BaseEntity {
  @Column(name = "customer_id")
  private int customerId;

  @Column(name = "quantity")
  private int quantity;

  @Column(name = "completed", columnDefinition = "boolean default false")
  private boolean completed;

  @Column(name = "completed_at", nullable = true)
  private Instant completedAt;

  @Enumerated(EnumType.STRING)
  @Column(name = "priority")
  Priority priority;
}
