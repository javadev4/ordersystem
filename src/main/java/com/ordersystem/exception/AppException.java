/**
 * @author Nil Panchal Mex IT Jun 6, 2022 4:16:39 PM AppException.java com.springdemo.exception
 *         SpringDemo
 */
package com.ordersystem.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.ordersystem.constant.Messages;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Nil Panchal Mex IT Jun 6, 2022 4:16:39 PM
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class AppException extends RuntimeException {
  private int code;
  private String type;
  private String message;

  public static AppException customerOrderExistsException() {
    return new AppException(HttpStatus.BAD_REQUEST.value(), Messages.FAIL,
        "Customer order already exists");
  }

}
