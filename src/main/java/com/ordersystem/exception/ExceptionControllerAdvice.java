/**
 * @author Nil Panchal Mex IT Jun 26, 2022 7:34:04 PM ExceptionControllerAdvice.java
 *         com.ordersystem.exception OrderSystem
 */
package com.ordersystem.exception;



import javax.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.ordersystem.constant.Messages;
import com.ordersystem.payload.response.ApiResponse;

/**
 * @author Nil Panchal Mex IT Jun 26, 2022 7:34:04 PM
 */
@ControllerAdvice
public class ExceptionControllerAdvice {
  @ExceptionHandler({ConstraintViolationException.class, MethodArgumentNotValidException.class,
      HttpMessageNotReadableException.class})
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ResponseEntity<ApiResponse> handleMethodArgumentNotValidException(Exception exception) {
    String errorMessage = "Invalid Request";
    if (exception instanceof HttpMessageNotReadableException) {
      errorMessage = "Invalid value";
    } else if (exception instanceof MethodArgumentNotValidException) {
      MethodArgumentNotValidException ex = (MethodArgumentNotValidException) exception;
      errorMessage = ex.getBindingResult().getFieldError().getDefaultMessage();
    }

    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ApiResponse.builder()
        .code(HttpStatus.BAD_REQUEST.value()).status(Messages.FAIL).message(errorMessage).build());
  }


  @ExceptionHandler(AppException.class)
  public ResponseEntity<ApiResponse> handleCustomException(AppException exception) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(MediaType.APPLICATION_JSON)
        .body(ApiResponse.builder().code(HttpStatus.BAD_REQUEST.value()).status(Messages.FAIL)
            .message(exception.getMessage()).build());
  }
}
