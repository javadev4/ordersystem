/**
 * @author Nil Panchal Mex IT Jun 26, 2022 8:14:27 PM OrderComparator.java
 *         com.ordersystem.comparator OrderSystem
 */
package com.ordersystem.comparator;

import java.util.Comparator;
import com.ordersystem.enums.Priority;
import com.ordersystem.model.Order;

/**
 * @author Nil Panchal Mex IT Jun 26, 2022 8:14:27 PM
 */
public class OrderComparator implements Comparator<Order> {

  @Override
  public int compare(Order o1, Order o2) {
    if (o1.getPriority().equals(Priority.HIGH) && o2.getPriority().equals(Priority.HIGH)) {
      if (o1.getCreatedAt().isBefore(o2.getCreatedAt())) {
        return 1;
      } else {
        return -1;
      }
    } else if (o1.getPriority().equals(Priority.HIGH) && o2.getPriority().equals(Priority.LOW)) {
      return 1;
    } else if (o1.getPriority().equals(Priority.LOW) && o2.getPriority().equals(Priority.HIGH)) {
      return -1;
    } else if (o1.getPriority().equals(Priority.LOW) && o2.getPriority().equals(Priority.LOW)) {
      if (o1.getCreatedAt().isBefore(o2.getCreatedAt())) {
        return 1;
      } else {
        return -1;
      }
    }
    return 0;
  }

}
