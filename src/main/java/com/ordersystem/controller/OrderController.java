/**
 * @author Nil Panchal Mex IT Jun 26, 2022 12:00:34 PM OrderController.java
 *         com.ordersystem.controller OrderSystem
 */
package com.ordersystem.controller;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.ordersystem.constant.PathVariableConstants;
import com.ordersystem.constant.URLConstants;
import com.ordersystem.payload.CreateOrderRequest;
import com.ordersystem.payload.response.ApiResponse;
import com.ordersystem.service.OrderService;

/**
 * @author Nil Panchal Mex IT Jun 26, 2022 12:00:34 PM
 */
@RestController
public class OrderController {

  
  private OrderService orderService;

  @Autowired
  public OrderController(OrderService orderService) {
    this.orderService = orderService;
  }

  /**
   * To create an order
   */
  @PostMapping(URLConstants.ORDERS)
  public ResponseEntity<ApiResponse> createOrder(@Valid @RequestBody CreateOrderRequest request) {
    ApiResponse response = orderService.createOrder(request);
    return ResponseEntity.status(response.getCode()).body(response);
  }

  /**
   * To get all orders
   */
  @GetMapping(URLConstants.ORDERS)
  public ResponseEntity<ApiResponse> getOrdersQueue() {
    ApiResponse response = orderService.getOrdersQueue();
    return ResponseEntity.status(response.getCode()).body(response);
  }

  /**
   * To get single order status
   */
  @GetMapping(URLConstants.SINGLE_ORDER)
  public ResponseEntity<ApiResponse> getCustomerOrderStatus(
      @Valid @PathVariable(value = PathVariableConstants.CLIENT_ID, required = true) int clientId) {
    ApiResponse response = orderService.getCustomerOrderStatus(clientId);
    return ResponseEntity.status(response.getCode()).body(response);
  }

  /**
   * To get next delivery order for Jim
   */
  @GetMapping(URLConstants.ORDERS_DELIVERY)
  public ResponseEntity<ApiResponse> getNextDeliveryOrders() {
    ApiResponse response = orderService.getNextDeliveryOrders();
    return ResponseEntity.status(response.getCode()).body(response);
  }

  /**
   * To delete customer order
   */
  @DeleteMapping(URLConstants.SINGLE_ORDER)
  public ResponseEntity<ApiResponse> cancelCustomerOrder(
      @Valid @PathVariable(name = PathVariableConstants.CLIENT_ID, required = true) int orderId) {
    System.out.println("orderId " + orderId);
    ApiResponse response = orderService.cancelCustomerOrder(orderId);
    return ResponseEntity.status(response.getCode()).body(response);
  }
}
