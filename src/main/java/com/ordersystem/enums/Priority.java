/**
 * @author Nil Panchal Mex IT Jun 26, 2022 9:28:13 PM Priority.java com.ordersystem.enums
 *         OrderSystem
 */
package com.ordersystem.enums;

import java.io.Serializable;

/**
 * @author Nil Panchal Mex IT Jun 26, 2022 9:28:13 PM
 */
public enum Priority implements Serializable {
  LOW, HIGH
}
